import React from 'react';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {Icon} from "vendor/bulma";
import {NavLink} from "react-router-dom";
import {NavbarDropdown} from "components/Dropdown";

const LoggedOutMenu = (props) => (
    <div className={props.expanded ? "navbar-menu is-active" : "navbar-menu"} onClick={props.onToggleExpand} id="navMenu">
        <div className="navbar-start">
            <NavLink className="navbar-item" activeClassName='is-active' exact to="/">
                <Icon medium>
                    <FontAwesomeIcon icon={'home'} />
                </Icon> Home
            </NavLink>
            <NavbarDropdown to='/discussions' link={() => (
                <>
                    <Icon medium>
                        <FontAwesomeIcon icon={'comment'} />
                    </Icon> Discuss
                </>
            )}>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'home'} />
                    </Icon> <span>Recent</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/topics" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'comments'} />
                    </Icon> <span>Topics</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/questions" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'question-circle'} />
                    </Icon> <span>Questions</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/links" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'link'} />
                    </Icon> <span>Links</span>
                </NavLink>
            </NavbarDropdown>
            <NavbarDropdown to='/explore' link={() => (
                <>
                    <Icon medium>
                        <FontAwesomeIcon icon={'globe-americas'} />
                    </Icon> Explore
                </>
            )}>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'calendar-check'} />
                    </Icon> <span>Today</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/live" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'play'} />
                    </Icon> <span>Live</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/products" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'ship'} />
                    </Icon> <span>Products</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/popular" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'fire'} />
                    </Icon> <span>Popular</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/makers">
                    <Icon medium>
                        <FontAwesomeIcon icon={'users'} />
                    </Icon> <span>Makers</span>
                </NavLink>
            </NavbarDropdown>
        </div>
        <div className="navbar-end">
            <NavLink className="navbar-item is-brand-green" activeClassName='is-active' to="/begin">
                <Icon medium>
                    <FontAwesomeIcon icon={'rocket'} />
                </Icon> Join us
            </NavLink>

            <NavLink className="navbar-item" activeClassName='is-active' to="/login">
                <Icon medium>
                    <FontAwesomeIcon icon={'sign-in-alt'} />
                </Icon> Log in
            </NavLink>
        </div>
    </div>
);

export default LoggedOutMenu;