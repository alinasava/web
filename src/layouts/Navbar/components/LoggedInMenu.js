import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";
import {Icon, Tag} from "vendor/bulma";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {NotificationsLink} from 'features/notifications';
import { Avatar } from "features/users";
import Streak from 'components/Streak';
import styled from 'styled-components';
import {NavbarDropdown} from "../../../components/Dropdown";

const StreakTag = styled(Tag)`
  background-color: #F3B283 !important;
  color: #6b330b !important;
  border-radius: 12px !important;
`

const LoggedInMenu = (props) => (
    <div className={props.expanded ? "navbar-menu is-active" : "navbar-menu"} onClick={props.onToggleExpand}>
        <div className="navbar-start">
            <NavLink className="navbar-item" activeClassName='is-active' to="/log">
                <Icon medium>
                    <FontAwesomeIcon icon={'check-square'} />
                </Icon> Log
            </NavLink>
            <NavbarDropdown to='/tasks' link={() => (
                <>
                    <Icon medium>
                        <FontAwesomeIcon icon={'check'} />
                    </Icon> Tasks
                </>
            )}>
                <NavLink className="navbar-item" activeClassName='is-active' to="/tasks" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'tasks'} />
                    </Icon> <span>Overview</span>
                </NavLink>

                <NavLink className="navbar-item" activeClassName='is-active' to="/products" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'ship'} />
                    </Icon> <span>Products</span>
                </NavLink>

                <NavLink className="navbar-item" activeClassName='is-active' to="/stats" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={['far', 'chart-bar']} />
                    </Icon> <span>Stats</span>
                </NavLink>

                <NavLink className="navbar-item" activeClassName='is-active' to="/apps" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'plug'} />
                    </Icon> <span>Apps</span>
                </NavLink>
            </NavbarDropdown>

            <NavbarDropdown to='/discussions' link={() => (
                <>
                    <Icon medium>
                        <FontAwesomeIcon icon={'comment'} />
                    </Icon> Discuss
                </>
            )}>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'home'} />
                    </Icon> <span>Recent</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/topics" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'comments'} />
                    </Icon> <span>Topics</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/questions" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'question-circle'} />
                    </Icon> <span>Questions</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/discussions/links" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'link'} />
                    </Icon> <span>Links</span>
                </NavLink>
            </NavbarDropdown>

            <NavbarDropdown to='/explore' link={() => (
                <>
                    <Icon medium>
                        <FontAwesomeIcon icon={'globe-americas'} />
                    </Icon> Explore
                </>
            )}>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'calendar-check'} />
                    </Icon> <span>Today</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/live" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'play'} />
                    </Icon> <span>Live</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/products" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'ship'} />
                    </Icon> <span>Products</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/popular" exact>
                    <Icon medium>
                        <FontAwesomeIcon icon={'fire'} />
                    </Icon> <span>Popular</span>
                </NavLink>
                <NavLink className="navbar-item" activeClassName='is-active' to="/explore/makers">
                    <Icon medium>
                        <FontAwesomeIcon icon={'users'} />
                    </Icon> <span>Makers</span>
                </NavLink>
            </NavbarDropdown>
        </div>
        <div className="navbar-end">
            <div className="navbar-item">
                <StreakTag rounded>
                    <Streak days={props.user.streak} />
                </StreakTag>
            </div>

            <NotificationsLink />


            <div className="navbar-item has-dropdown is-hoverable">
                {
                    // eslint-disable-next-line
                } <a className="navbar-link">
                    <Avatar is={24} user={props.user} />
                </a>

                <div className="navbar-dropdown is-right">
                    <NavLink className="navbar-item" to={`/@${props.user.username}`}>
                        You
                    </NavLink>

                    <NavLink className="navbar-item" to={`/chat`}>
                        Chat
                    </NavLink>

                    <a className={"navbar-item"} href={"https://bugs.getmakerlog.com"} target={'_blank'} rel="noopener noreferrer">
                        Bugs
                    </a>

                    <a className={"navbar-item"} href={"https://ideas.getmakerlog.com"} target={'_blank'} rel="noopener noreferrer">
                        Ideas
                    </a>

                    <NavLink className="navbar-item" to={`/settings`}>
                        Settings
                    </NavLink>

                    <a className="navbar-item" href={"/logout"} onClick={e => { e.preventDefault(); props.onClickLogout()}}>
                        Sign out
                    </a>
                </div>
            </div>
        </div>
    </div>
)

LoggedInMenu.propTypes = {
    onClickLogout: PropTypes.func.isRequired,
    isSyncing: PropTypes.bool.isRequired,
    user: PropTypes.shape({
        avatar: PropTypes.string,
    })
}

export default LoggedInMenu;