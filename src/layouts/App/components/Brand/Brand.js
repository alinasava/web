import React from 'react';
import './style.css';
import {Icon, Image} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";

const Brand = (props) => (
    <div className="brand-badge tags has-addons is-hidden-mobile">
            <span className="tag is-light is-medium">
                <Image is="24x24" src="https://pbs.twimg.com/profile_images/907090174689017856/opAzAcbp_400x400.jpg" />
            </span>
            <span className="tag is-white is-medium">
                <a href="https://sergiomattei.com" target="_blank" rel="noopener noreferrer">
                    <Icon><FontAwesomeIcon icon={'globe'} /></Icon>
                </a>
            </span>
            <span className="tag is-white is-medium">
                <a href="https://twitter.com/matteing" target="_blank" rel="noopener noreferrer">
                    <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon>
                </a>
            </span>
            <span className="tag is-white is-medium">
                <a href="https://soundcloud.com/sergio-diaz-146/sets/makerlog" target="_blank" rel="noopener noreferrer">
                    <Icon><FontAwesomeIcon icon={['fab', 'soundcloud']} /></Icon>
                </a>
            </span>
    </div>
);

export default Brand;