import React from 'react';
import Navbar from './Navbar';
import LoadOverlay from '../components/LoadOverlay';
import Footer from './Footer';

const Page = ({ nav=true, footer=true, contained=true, dark=false, className=null, loading=false, errored=false, transparent=false, translucent=false, ...props }) => {
    if (loading || errored) {
        return (
            <div id="page" className={'is-loading'}>
                {nav && <Navbar dark={dark} />}
                {(loading || errored) && <LoadOverlay coverParent errored={errored} />}
            </div>
        )
    }

    return (
        <div id="page">
            <div id={"page-content"} className={`${className}`}>
                {nav && <Navbar dark={dark} transparent={transparent} translucent={translucent} />}
                {contained ?
                    <div className="container" style={{paddingTop: 30}}>
                        {props.children}
                    </div>
                    :
                    props.children
                }
            </div>
            {footer && <Footer />}
        </div>
    )
}

export default Page