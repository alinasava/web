import React from 'react';
import PropTypes from 'prop-types';
import {Message} from "vendor/bulma";

const renderFieldErrors = (fieldErrors) => {
    if (fieldErrors !== null && typeof fieldErrors === 'object') {
        let list = []
        /*let errors = Object.keys(fieldErrors).map(
            k => {
                list.push(
                    <ul><b>{k}</b>: {fieldErrors[k]}</ul>
                )
            }
        ) */

        return <ul>{list}</ul>
    }

    return null;
}

const ErrorMessageList = ({ errorMessages, fieldErrors }) => (
    <Message danger>
        <Message.Body>
            <ul>
                <b>{errorMessages && errorMessages.map((message) => <li key={message}>{message}</li>)}</b>
                {errorMessages && fieldErrors && <hr />}
                {fieldErrors &&
                    <div>
                        {renderFieldErrors(fieldErrors)}
                    </div>
                }
            </ul>
        </Message.Body>
    </Message>
);

ErrorMessageList.propTypes = {
    errorMessages: PropTypes.array.isRequired,
}

export default ErrorMessageList;