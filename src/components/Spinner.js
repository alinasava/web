import React from 'react';
import ClipLoader from 'vendor/ClipLoader';
import ClimbingBoxLoader from 'vendor/ClimbingBoxLoader';
import PropTypes from 'prop-types';

const Spinner = ({ color = "black", small = false, text = null }) => {
    if (small) {
        return <div className={"SmallSpinner"}><ClipLoader size={15} color={color} /> {text}</div>;
    } else {
        return (
            <div>
                <ClimbingBoxLoader color={color} />
                <br />
                <small style={{ color: color }}>
                    {text}
                </small>
            </div>
        )
    }
}

Spinner.propTypes = {
    color: PropTypes.string,
    small: PropTypes.bool,
}

export default Spinner;