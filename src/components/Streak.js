import React from 'react';
import PropTypes from 'prop-types';
import Emoji from 'components/Emoji';

const Streak = ({ days, endingSoon = false, className="" }) => (
    <div className={className}>
        <Emoji emoji={days ? "🔥" : "😢"} />&nbsp;{days}{endingSoon ? <Emoji emoji="⌛" /> : null}
    </div>
)

Streak.propTypes = {
    days: PropTypes.number.isRequired,
    endingSoon: PropTypes.bool,
}

export default Streak;