import React from 'react'

class CarbonAd extends React.Component {
    componentDidMount() {
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.id = '_carbonads_js';
        s.src = `https://cdn.carbonads.com/carbon.js?serve=CK7DC5QJ&placement=${this.props.placement}`;
        this.instance.appendChild(s);
    }

    render() {
        return <div ref={el => (this.instance = el)}></div>;
    }
}


CarbonAd.defaultProps = {
    placement: 'getmakerlogcom'
}

export default CarbonAd;