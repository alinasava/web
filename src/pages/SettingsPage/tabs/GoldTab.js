import React from 'react';
import {
   Control,
    Field,
} from "vendor/bulma";
import {me, patchSettings} from "lib/user";
import {ChromePicker} from "react-color";

class GoldTab extends React.Component {
    state = {
        loading: true,
        isPosting: false,
        accent: '',
        failed: false,
    }

    componentDidMount() {
        this.prefillFields()
    }

    onSubmit = async (event) => {
        event.preventDefault();
        this.setState({ loading: true })
        try {
            await patchSettings({
                accent: this.state.accent,
            })
            this.setState({ loading: false })
        } catch (e) {

        }
    }


    prefillFields = async () => {
        try {
            const data = await me();
            this.setState({
                loading: false,
                accent: data.accent,
            })
        } catch (e) {

        }
    }

    onChangeColor = async (color, event) => {
        await this.setState({
            accent: color.hex,
        })
        await this.onSubmit(event)
        this.props.changeAccent(color.hex);
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }

            return messages
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            return <p>{errors}</p>;
        }

        return messages
    }

    render() {
        return (
            <div>
                <label className="label">Accent color</label>
                <Field hasAddons>
                    <Control>
                        <ChromePicker disableAlpha color={this.state.accent} onChangeComplete={this.onChangeColor} />
                    </Control>
                </Field>
            </div>
        )
    }
}

export default GoldTab;