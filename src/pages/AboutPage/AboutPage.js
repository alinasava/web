import React from 'react';
import { actions as userActions } from 'ducks/user';
import { connect } from 'react-redux';
import {
    Box,
    Card, Container, Heading, Hero, Icon, Image, Level, Media, SubTitle,
    Tabs,
    Title,
    Content
} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";
import Page from "layouts/Page";
import Emoji from "../../components/Emoji";

const About = () => (
    <div>
        <strong>Makerlog is a task log for makers that gets you more motivated by the more you make.</strong>
        <br />
        I made it to keep me motivated when building products.
        <hr />
        <Box>
            <Media>
                <Media.Left>
                    <Image is='64x64' src="https://sergiomattei.com/img/me.jpg" alt="Image"/>
                </Media.Left>
                <Media.Content>
                    <Content>
                        <p>
                            <strong>Sergio Mattei Díaz</strong><br />
                            I make products people love.
                        </p>
                    </Content>
                    <Level mobile>
                        <Level.Left>
                            <Level.Item>
                                <Link to={'/@sergio'}>
                                    <Heading>
                                        <Icon><FontAwesomeIcon icon={'check-circle'} /></Icon>
                                        <span>Makerlog</span>
                                    </Heading>
                                </Link>
                            </Level.Item>
                            <Level.Item>
                                <a href="https://twitter.com/matteing">
                                    <Heading>
                                        <Icon><FontAwesomeIcon icon={['fab', 'twitter']} /></Icon>
                                        <span>Twitter</span>
                                    </Heading>
                                </a>
                            </Level.Item>
                            <Level.Item>
                                <a href="https://sergiomattei.com">
                                    <Heading>
                                        <Icon><FontAwesomeIcon icon={'globe'} /></Icon>
                                        <span>Website</span>
                                    </Heading>
                                </a>
                            </Level.Item>
                        </Level.Left>
                    </Level>
                </Media.Content>
            </Media>
        </Box>
    </div>
)

const TermsOfService = () => (
    <Content>
        <h2>Makerlog Terms of Service</h2>
        <h3>1. Terms</h3>
        <p>By accessing the website at <a href="http://getmakerlog.com">http://getmakerlog.com</a>, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.</p>
        <h3>2. Use License</h3>
        <ol type="a">
            <li>Permission is granted to temporarily download one copy of the materials (information or software) on Makerlog's website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                <ol type="i">
                    <li>modify or copy the materials;</li>
                    <li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
                    <li>attempt to decompile or reverse engineer any software contained on Makerlog's website;</li>
                    <li>remove any copyright or other proprietary notations from the materials; or</li>
                    <li>transfer the materials to another person or "mirror" the materials on any other server.</li>
                </ol>
            </li>
            <li>This license shall automatically terminate if you violate any of these restrictions and may be terminated by Makerlog at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</li>
        </ol>
        <h3>3. Disclaimer</h3>
        <ol type="a">
            <li>The materials on Makerlog's website are provided on an 'as is' basis. Makerlog makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</li>
            <li>Further, Makerlog does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</li>
        </ol>
        <h3>4. Limitations</h3>
        <p>In no event shall Makerlog or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Makerlog's website, even if Makerlog or a Makerlog authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
        <h3>5. Accuracy of materials</h3>
        <p>The materials appearing on Makerlog's website could include technical, typographical, or photographic errors. Makerlog does not warrant that any of the materials on its website are accurate, complete or current. Makerlog may make changes to the materials contained on its website at any time without notice. However Makerlog does not make any commitment to update the materials.</p>
        <h3>6. Links</h3>
        <p>Makerlog has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Makerlog of the site. Use of any such linked website is at the user's own risk.</p>
        <h3>7. Modifications</h3>
        <p>Makerlog may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.</p>
        <h3>8. Governing Law</h3>
        <p>These terms and conditions are governed by and construed in accordance with the laws of Puerto Rico and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</p>
        <h2>Privacy Policy</h2>
        <p>Your privacy is important to us. It is Makerlog's policy to respect your privacy regarding any information we may collect from you across our website, <a href="http://getmakerlog.com">http://getmakerlog.com</a>, and other sites we own and operate.</p>
        <p>We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.</p>
        <p>We only retain collected information for as long as necessary to provide you with your requested service. What data we store, we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorised access, disclosure, copying, use or modification.</p>
        <p>We don’t share any personally identifying information publicly or with third-parties, except when required to by law.</p>
        <p>Our website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.</p>
        <p>You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.</p>
        <p>Your continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.</p>
        <p>This policy is effective as of 13 June 2018.</p>
    </Content>
)

const FrequentlyAsked = props => (
    <Content>
        <h3>What makes Makerlog different to, say, products like WIP?</h3>
        <p>
            Great question! Makerlog was initially based on WIP's concept, and started out as a small personal clone. After a while, I began to let people into Makerlog and started adding features that differentiated from others.
            I wrote a few bullets that sums it up pretty well:

            <ul>
                <li>
                    I needed it to integrate with my workflow transparently. Makerlog is API centric and easy to integrate with your own apps. You can post events from anywhere!
                </li>
                <li>Free. As a student, I couldn't afford WIP.chat, so I built my own.</li>
                <li>Stats. I wanted something with great stats, because that's what productivity tools do.</li>
            </ul>
        </p>
        <h3>How do Makerlog streaks work?</h3>
        <p>
            In bullets...
            <ul>
                <li><b>You must add at least one task before 12AM in your timezone.</b></li>
                <li><b>Remaining tasks are not counted.</b></li>
                <li><b>The task completion time is what matters, not creation time.</b> If you added a task yesterday and completed it today, it was marked as done today and counts for today's streak.</li>
            </ul>
        </p>
        <h3>Help, I lost my streak!</h3>
        <p>
            <Emoji emoji={"😔"}/> <b>Uh oh! Well, better luck next time!</b> <br />
            If you believe this was in error, you can contact me and I may be able to restore it, but <b>this is extremely rare</b>. Valid reasons below...
            <ul>
                <li><b>Travel or loss of internet access.</b> You must prove you got something done.</li>
                <li><b>Algorithm issues.</b> It happens, my code breaks sometimes due to timezone changes. </li>
            </ul>
        </p>
        <h3>I want that shiny tester badge!</h3>
        <p>
            The tester badge is rarely granted nowadays (mostly because I grant it manually), but if you report a valid bug on the <a href={"https://bugs.getmakerlog.com"}>bug tracker</a> and ask nicely, I'll be happy to grant it!
        </p>
    </Content>
)

class AboutPage extends React.Component {
    state = {
        activeTab: 1,
    }

    switchToTab = (index) => {
        this.setState({
            activeTab: index
        })
    }

    renderTabLink = (name, index) => (
        <li
            className={this.state.activeTab === index ? "is-active" : null}> {/* eslint-disable-next-line */}
            <a onClick={() => this.switchToTab(index)}>{name}</a>
        </li>
    )

    render() {
        return (
            <Page contained={false} className="AboutPage">
                <Hero primary>
                    <Hero.Body>
                        <Container>
                            <Title is={"4"}>
                                About Makerlog
                            </Title>
                            <SubTitle>
                                A little bit about my product...
                            </SubTitle>
                        </Container>
                    </Hero.Body>
                </Hero>
                <br />
                <Container>
                    <Card>
                        <Card.Header>
                            <Tabs large>
                                <ul>
                                    {this.renderTabLink("About", 1)}
                                    {this.renderTabLink("Frequently Asked Questions", 2)}
                                    {this.renderTabLink("Terms of Service & Privacy", 3)}
                                </ul>
                            </Tabs>
                        </Card.Header>
                        <Card.Content>
                            {this.state.activeTab === 1 && <About />}
                            {this.state.activeTab === 2 && <FrequentlyAsked />}
                            {this.state.activeTab === 3 && <TermsOfService />}
                        </Card.Content>
                    </Card>
                </Container>
            </Page>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchUser: () => dispatch(userActions.loadUser())
})

const mapStateToProps = (state) => ({
    isLoading: state.user.isLoading,
    user: state.user.me,
})

AboutPage.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutPage);