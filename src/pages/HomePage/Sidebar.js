import React from 'react';
import { PeopleCard } from 'features/users';
import AppCard from "../../components/sidebar/AppCard";
import TrendingDiscussionsCard from "../../components/sidebar/TrendingDiscussionsCard";
import AdCard from "../../components/sidebar/AdCard";
import {LoggedOutOnly} from "features/users";
import RegisterCard from "components/sidebar/RegisterCard";
import Sticky from 'react-stickynode';

const Sidebar = () => {
    return (
        <div>
            <PeopleCard withDiscussions={false} recentlyLaunched />
            <br/>
            <TrendingDiscussionsCard />
            <br />
            <AppCard />
            <br />
            <AdCard />
            <br />
            <Sticky top={30}>
                <LoggedOutOnly>
                    <RegisterCard />
                </LoggedOutOnly>
            </Sticky>
        </div>
    );
}

export default Sidebar;
