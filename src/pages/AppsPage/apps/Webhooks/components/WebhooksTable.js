import React from 'react';
import {Button, Table} from 'vendor/bulma';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { getAppIcon } from 'lib/apps';
import {deleteHook} from "../../../../../lib/integrations/webhook";

class DeleteButton extends React.Component {
    state = {
        deleting: false,
        failed: false,
    }

    onClick = async () => {
        try {
            this.setState({ deleting: true, failed: false })
            await deleteHook(this.props.id)
            this.setState({ deleting: false, failed: false })
            if (this.props.onDelete) {
                this.props.onDelete(this.props.id)
            }
        } catch (e) {
            this.setState({ deleting: false, failed: true })
        }
    }

    render() {
        return (
            <Button
                danger
                onClick={this.onClick}
                loading={this.state.deleting}>
                {this.state.failed ? 'Failed to delete.' : 'Delete hook'}
            </Button>
        )
    }
}

const WebhookRow = ({ webhook, onDelete }) => (
    <Table.Tr>
        <Table.Th><FontAwesomeIcon icon={getAppIcon(webhook.event)} /></Table.Th>
        <Table.Td>{webhook.token_preview}</Table.Td>
        <Table.Td>{webhook.project ? webhook.project.name : "No project attached"}</Table.Td>
        <Table.Td>{webhook.description}</Table.Td>
        <Table.Td>{webhook.extra_data || "No data."}</Table.Td>
        <Table.Td><DeleteButton id={webhook.id} onDelete={onDelete} /></Table.Td>
    </Table.Tr>
)

class WebhookTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            webhooks: this.props.webhooks
        }
    }

    deleteHook = (id) => {
        this.setState({
            webhooks: this.state.webhooks.filter(h => h.id !== id)
        })
    }

    render() {
        const webhooks = this.state.webhooks;

        return (
            <Table className="is-fullwidth">
                <Table.Head>
                    <Table.Tr>
                        <Table.Th><abbr title="Application">App</abbr></Table.Th>
                        <Table.Th>Token (secret)</Table.Th>
                        <Table.Th>Linked project</Table.Th>
                        <Table.Th>Description</Table.Th>
                        <Table.Th>App data</Table.Th>
                        <Table.Th>Actions</Table.Th>
                    </Table.Tr>
                </Table.Head>
                <Table.Body>
                    {webhooks && webhooks.map(
                        webhook => <WebhookRow webhook={webhook} onDelete={this.deleteHook} />
                    )}
                </Table.Body>
            </Table>
        )
    }
}

export {
    WebhookRow,
    WebhookTable
}