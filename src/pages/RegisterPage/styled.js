import styled from 'styled-components';
import Page from "layouts/Page";

const RegisterPageLayout = styled(Page)`
    .card {
        border: none !important;
    }
    
    .card-footer {
         border-bottom-left-radius: 8px;
        border-bottom-right-radius: 8px;
    }
`

export {
    RegisterPageLayout
}