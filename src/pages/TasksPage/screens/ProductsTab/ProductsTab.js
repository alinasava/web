import React from 'react';
import { Switch as RouteSwitch, Route } from 'react-router-dom';
import {
    Hero, SubTitle, Button, Card,
} from 'vendor/bulma';
import Emoji from 'components/Emoji';
import { getMyProducts } from 'lib/products';
import Spinner from "../../../../components/Spinner";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { Product, ProductCreateModal, ProductGrid} from "features/products";

const NoProductsFound = (props) => (
    <Hero className={"has-text-centered"}>
        <Hero.Body>
            <SubTitle is='3'>
                <strong className="has-text-grey">You haven't created any products.</strong>
                <SubTitle is='5'>
                    <strong className="has-text-grey-light">
                        Track your progress, unify your project logs, and showcase your creations all in one place. 
                    </strong>
                </SubTitle>
            </SubTitle>
        </Hero.Body>
        <Hero.Foot>
            <Button onClick={props.onClickCreate} primary medium>
                <Emoji emoji="✨" /> Add a product
            </Button>
        </Hero.Foot>
    </Hero>
)

const CreateIconCard = (props) => (
    <Card className="CreateIconCard" onClick={props.onClick}>
        <Card.Content>
            <p className="icon">
                <FontAwesomeIcon icon={'plus'} />
            </p>
            <p>
                <SubTitle>
                    Add a product
                </SubTitle>
            </p>
        </Card.Content>
    </Card>
)

class ProductsHome extends React.Component {
    state = {
        ready: false,
        products: [],
        failed: false,
        isCreateModalOpen: false,
        isEditModalOpen: false,
    }

    toggleCreateModal = () => {
        this.setState({
            isCreateModalOpen: !this.state.isCreateModalOpen
        })
    }

    toggleEditModal = () => {
        this.setState({
            isEditModalOpen: !this.state.isEditModalOpen
        })
    }

    fetchProducts = async () => {
        try {
            const products = await getMyProducts();
            this.setState({ ready: true, products: products, failed: false })
        } catch (e) {
            this.setState({ ready: true, failed: true, products: [] })
        }
    }

    onCreateProduct = () => {
        this.toggleCreateModal();
        this.fetchProducts();
    }

    onEditProduct = () => {
        this.fetchProducts();
    }

    componentDidMount() {
        this.fetchProducts()
    }

    renderProducts = () => {
        if (!this.state.ready) {
            return (
                <center>
                    <Spinner text={"Loading your products..."} />
                </center>
            )
        }

        if (!this.state.failed && this.state.products.length === 0) {
            return <NoProductsFound onClickCreate={this.toggleCreateModal} />;
        }

        return (
            <ProductGrid>
                {this.state.products.map(
                    (product) => (
                        <Product card product={product} key={product.id} onEdit={this.onEditProduct} onDelete={this.onEditProduct} />
                    )
                )}
                <div>
                    <CreateIconCard onClick={this.toggleCreateModal} />
                </div>
            </ProductGrid>
        )
    }


    render() {
        return (
            <div className="ProductsHome">
                {this.renderProducts()}
                <ProductCreateModal
                    open={this.state.isCreateModalOpen}
                    onClose={this.toggleCreateModal}
                    onFinish={this.onCreateProduct} />
            </div>
        )
    }
}

const ProductsTab = (props) => (
    <div className="ProductsTab">
        <RouteSwitch>
            <Route exact path='/tasks/products' component={ProductsHome} />
        </RouteSwitch>
    </div>
)

ProductsTab.propTypes = {}

export default ProductsTab;