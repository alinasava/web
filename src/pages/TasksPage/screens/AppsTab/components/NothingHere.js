import React from 'react';
import {Button, Hero, Icon, SubTitle, Title} from "vendor/bulma";
import {Link} from "react-router-dom";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

const NothingHere = (props) => (
    <Hero className={"has-text-centered"}>
        <Hero.Body>
            <Title spaced className="has-text-grey">
                <Icon large><FontAwesomeIcon icon={['fab', 'trello']}/></Icon>
                &nbsp;
                <Icon large><FontAwesomeIcon icon={['fab', 'github-square']}/></Icon>
                &nbsp;
                <Icon large><FontAwesomeIcon icon={['fab', 'slack']}/></Icon>
            </Title>
            <SubTitle is='3'>
                <strong className="has-text-grey">You have no apps.</strong>
                <SubTitle is='5'>
                    <strong className="has-text-grey-light">Makerlog seamlessly aggregates data from all your favorite services.</strong>
                </SubTitle>
            </SubTitle>
        </Hero.Body>
        <Hero.Foot>
            <Link to="/apps">
                <Button primary medium>App Shop</Button>
            </Link>
        </Hero.Foot>
    </Hero>
);

export default NothingHere;
