import React from 'react';
import {Box, Button, Icon, Image, Level, Media, Content} from "vendor/bulma";
import {Link} from "react-router-dom";
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

const SetupButton = ({ installed }) => {
    if (installed) {
        return (
            <Button small>
                <Icon><FontAwesomeIcon icon={'cogs'} /></Icon>
                <span>Settings</span>
            </Button>
        )
    } else {
        return (
            <Button small primary>
                <Icon><FontAwesomeIcon icon={'plug'} /></Icon>
                <span>Install</span>
            </Button>
        )
    }
}

const ServiceCard = (props) => (
    <Box>
        <Media>
            <Media.Left>
                <Image is='64x64' src={props.logo} alt={props.name} />
            </Media.Left>
            <Media.Content>
                <Content>
                    <p>
                        <strong>{props.name}</strong><br /><small>{props.summary}</small>
                        <br/>
                    </p>
                </Content>
                <Level mobile>
                    <Level.Left>
                        <Level.Item>
                            <Link to={props.to}>
                                <SetupButton installed={props.installed} />
                            </Link>
                        </Level.Item>
                    </Level.Left>
                </Level>
            </Media.Content>
        </Media>
    </Box>
)

ServiceCard.propTypes = {}

export default ServiceCard;