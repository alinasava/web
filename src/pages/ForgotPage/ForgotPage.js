import React from 'react';
import Quote from "../../components/Quote";
import {Button, SubTitle, Title} from "vendor/bulma";
import queryString from "query-string";
import axios from 'axios';
import {Redirect} from "react-router-dom";
import ErrorMessageList from "../../components/forms/ErrorMessageList";
import {prettyAxiosError} from "../../lib/utils/error";
import Page from "../../layouts/Page";

class ResetForm extends React.Component {
    state = {
        password: '',
        repeatPassword: '',
        success: false,
        loading: false,
        failed: false,
        errorMessages: null,
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.resetPassword()
    }

    resetPassword = async () => {
        try {
            this.setState({ success: false, failed: false, loading: true })
            const uid = this.props.uid;
            const token = this.props.token;

            await axios.post('/accounts/reset/', {
                uidb64: uid,
                token: token,
                repeat_password: this.state.repeatPassword,
                password: this.state.password,
            })
            this.setState({ success: true, failed: false, loading: false })
        } catch (e) {
            try {
                prettyAxiosError(e)
            } catch (e) {
                if (e.field_errors) {
                    this.setState({
                        failed: true,
                        success: false,
                        loading: false,
                        errorMessages: e.field_errors
                    })
                } else {
                    this.setState({
                        failed: true,
                        success: false,
                        loading: false,
                        errorMessages: null,
                    })
                }
            }
        }
    }

    render() {
        if (this.state.success) {
            return <Redirect to={'/login'} />
        }

        return (
            <form onSubmit={this.onSubmit}>
                {this.state.failed && this.state.errorMessages &&
                    <ErrorMessageList fieldErrors={this.state.errorMessages} />
                }
                <input
                    className="transparent-input"
                    type={"password"}
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                    placeholder="Password" />
                <br />
                <input
                    className="transparent-input"
                    type={"password"}
                    value={this.state.repeatPassword}
                    onChange={e => this.setState({ repeatPassword: e.target.value })}
                    placeholder="Repeat password" />
                <center>
                    <Button
                        onClick={this.onSubmit}
                        type="submit"
                        loading={this.state.loading}
                        className="is-brand-green nice-button">
                        Change password &raquo;
                    </Button>
                </center>
            </form>
        )
    }
}

class ForgotForm extends React.Component {
    state = {
        email: '',
        success: false,
        loading: false,
        failed: false,
    }

    onSubmit = async (e) => {
        e.preventDefault()
        try {
            this.setState({
                loading: true,
                success: false,
                failed: false,
            })
            await axios.post('/accounts/forgot/', {
                email: this.state.email
            })
            this.setState({
                email: '',
                success: true,
                loading: false,
                failed: false,
            })
        } catch (e) {
            this.setState({
                success: false,
                loading: false,
                failed: true,
            })
        }
    }

    render() {
        if (this.state.success || this.state.failed) {
            return <SubTitle is={"5"} className={"has-text-white"}>If an email with this address exists, we've sent an email.</SubTitle>
        }

        return (
            <form onSubmit={this.onSubmit}>
                <input
                    className="transparent-input"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                    placeholder="Email" />
                <br />
                <center>
                    <Button
                        onClick={this.onSubmit}
                        type="submit"
                        loading={this.state.loading}
                        className="is-brand-green nice-button">
                        Reset &raquo;
                    </Button>
                </center>
            </form>
        )
    }
}

class ForgotPage extends React.Component {
    state = {
        hasCode: false,
    }

    componentDidMount() {
        let params = queryString.parse(this.props.location.search)
        this.params = params;
        if (params.uid && params.token) {
            this.setState({ hasCode: true })
        }
    }

    render() {
        return (
            <Page contained={false} className="quote-page">
                <div className="columns is-fullheight accounts-hero">
                    <div className="column content-column is-two-fifths">
                        <Title is={"2"}>Forgot password?</Title>

                        <center>
                            {this.state.hasCode ? <ResetForm uid={this.params.uid} token={this.params.token} /> : <ForgotForm />}
                        </center>
                    </div>
                    <div className="column tint">
                        <Quote
                            quote="I love helping people reset their passwords."
                            author="Makerlog's Forgot Page"
                        />
                    </div>
                </div>
            </Page>
        )
    }
}

ForgotPage.propTypes = {}

export default ForgotPage;