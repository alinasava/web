import React from 'react';
import {connect} from "react-redux";
import { actions as streamActions } from 'ducks/stream';
import NoActivityCard from "../../features/stream/components/NoActivityCard/NoActivityCard";
import {PeopleCard} from "features/users";
import {MyProductsCard} from 'features/products';
import {FollowingStream} from "features/stream";
import Page from "layouts/Page";
import RecentDiscussionsCard from "components/sidebar/RecentDiscussionsCard";
import StreamHeader from "./components/StreamHeader";
import AdCard from "../../components/sidebar/AdCard";
import FooterCard from "components/sidebar/FooterCard";
import AppCard from "../../components/sidebar/AppCard";

class StreamPage extends React.Component {
    render() {
        if ((this.props.data.length === 0 && this.props.initialLoaded && !this.props.isSyncing) || this.props.isNewUser) {
            // Onboarding procedures will hide everything in the page for design reasons.
            return (
                <Page contained={false} footer={false}>
                    <StreamHeader />
                    <div className="container">
                        <div className="columns">
                            <div className="column">
                                <NoActivityCard />
                            </div>
                            <div className="column" style={{maxWidth: '400px', marginTop: 54}}>
                                <PeopleCard recentlyLaunched />
                                <br />

                                <RecentDiscussionsCard />
                                <br />

                                <MyProductsCard />
                                <hr />
                                <AdCard />
                                <br />
                                <AppCard />
                                <br />
                                <FooterCard />

                            </div>
                        </div>
                    </div>
                </Page>
            )
        }

        return (
            <Page contained={false} footer={false}>
                <StreamHeader />

                <div className="container">
                    <div className="columns">
                        <div className="column">
                            <FollowingStream />
                        </div>
                        <div className="column" style={{maxWidth: '400px', marginTop: 54}}>
                            <PeopleCard recentlyLaunched />
                            <br />

                            <RecentDiscussionsCard />
                            <br />

                            <MyProductsCard />
                            <hr />
                            <AdCard />
                            <br />
                            <AppCard />
                            <br />
                            <FooterCard />

                        </div>
                    </div>
                </div>
            </Page>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        isNewUser: state.app.isNewUser,
        isSyncing: state.stream.isSyncing,
        errorMessages: state.stream.errorMessages,
        data: state.stream.data,
        allLoaded: state.stream.allLoaded,
        fetchFailed: state.stream.fetchFailed,
        initialLoaded: state.stream.initialLoaded,
        hasMore: state.stream.nextUrl // rely on the nextUrl being truthy
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        init: () => dispatch(streamActions.init()),
        loadMore: () => dispatch(streamActions.loadMore())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StreamPage);