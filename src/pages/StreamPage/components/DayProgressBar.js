import React from 'react';
import styled from 'styled-components';
import {darker} from "styles/colors";
import {Tooltip} from "react-tippy";
import Emoji from "components/Emoji";

const ProgressBar = styled.div`
  position: absolute;
  background-color: ${props => darker(props.theme.primaryDarker)};
  width: ${props => props.progress ? props.progress : 0}%;
  transition: width 1s ease-in-out;
  height: 5px;
  bottom: 0;
  left: 0;
  z-index: 10;
  cursor: pointer;
  transform: translateZ(0);
  will-change: transform;
`

class DayProgressBar extends React.Component {
    state = {
        percentage: null
    }

    componentDidMount() {
        this.check = setInterval(this.refresh, 10000)
    }

    componentWillUnmount() {
        clearInterval(this.check)
    }

    refresh = () => {
        this.setState({
            percentage: this.getDayProgress()
        })
    }

    getDayProgress = () => {
        const dt = new Date();
        const secs = dt.getSeconds() + (60 * (dt.getMinutes() + (60 * dt.getHours())));
        return (secs / 86400) * 100
    }

    render() {
        return (
            <Tooltip
                // options
                html={
                    <small>
                        <Emoji emoji={"🔥"} /> Day progress (until streaks end)
                    </small>
                }
                delay={500}
                followCursor
                position="bottom-end"
                theme={'light'}
            >
            <ProgressBar progress={this.state.percentage} />
            </Tooltip>
        )
    }
}

export default DayProgressBar;