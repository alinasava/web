import React from 'react';
import {Route} from 'react-router-dom';
import {Button} from 'vendor/bulma';
import Navigation from "./components/Navigation/Navigation";
import {Switch} from "react-router";
import NotFound from "../Error/NotFound/NotFound";
import { getWorldStats } from "../../lib/stats";
import MakersTab from "./components/MakersTab";
import TodayTab from "./components/TodayTab";
import PopularTab from './components/PopularTab/index';
import AllProductsTab from './components/AllProductsTab';
import Page from 'layouts/Page';

class ExplorePage extends React.Component {
    state = {
        isLoading: true,
        ready: false,
        failed: false,
        data: null,
    }

    componentDidMount() {
        this.fetchStats();
    }

    fetchStats = async () => {
        try {
            const stats = await getWorldStats();
            this.setState({ isLoading: false, ready: true, failed: false, data: stats })
        } catch (e) {
            this.setState({ isLoading: false, failed: true })
        }
    }

    render() {

        return (
            <Page loading={this.state.isLoading} contained={false}>
                <Route component={Navigation} />
                {this.state.failed &&
                    <div>
                        Failed to load this page. <Button onClick={this.fetchStats}>Retry</Button>
                    </div>
                }
                {this.state.ready && this.state.data && !this.state.failed &&
                    <div>
                        <Switch>
                            <Route exact path='/explore' component={() => <TodayTab worldStats={this.state.data} />} />
                            <Route exact path='/explore/products' component={() => <AllProductsTab worldStats={this.state.data} />} />
                            <Route exact path='/explore/popular' component={() => <PopularTab worldStats={this.state.data} />} />
                            <Route path='/explore/makers' component={() => <MakersTab topUsers={this.state.data.top_users} />} />
                            <Route component={NotFound} />
                        </Switch>
                    </div>
                }
            </Page>
        )
    }
}

ExplorePage.propTypes = {}

export default ExplorePage;