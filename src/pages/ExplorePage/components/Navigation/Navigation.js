import React from 'react';
import {Icon} from 'vendor/bulma';
import  { NavLink } from 'react-router-dom';
import './Navigation.css';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import LoggedInOnly from "../../../../features/users/containers/LoggedInOnly";

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false,
        }
    }

    onClickCloseExpand = () => {
        if (this.state.expanded) {
            this.setState({ expanded: !this.state.expanded })
        }
    }

    render() {
        return (
            <nav className="navbar navbar-secondary is-white Navigation" aria-label="main navigation">
                <div className={"container"}>
                    <div className="navbar-brand">
                        <h1 className="navbar-item">
                            <span className={"title is-5 brand-underline"}>Explore</span>
                        </h1>

                        <div className="navbar-burger" onClick={e => this.setState({ expanded: !this.state.expanded })}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div className={this.state.expanded ? "navbar-menu is-active" : "navbar-menu"}>
                        <div className="navbar-start" onClick={this.onClickCloseExpand}>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/explore" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'calendar-check'} />
                                </Icon> <span>Today</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/live" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'play'} />
                                </Icon> <span>Live</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/explore/products" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'ship'} />
                                </Icon> <span>Products</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/explore/popular" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={'fire'} />
                                </Icon> <span>Popular</span>
                            </NavLink>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/explore/makers">
                                <Icon medium>
                                    <FontAwesomeIcon icon={'users'} />
                                </Icon> <span>Makers</span>
                            </NavLink>
                        </div>

                        <div className="navbar-end" onClick={this.onClickCloseExpand}>
                            <NavLink className="navbar-item" activeClassName='is-active' to="/open" exact>
                                <Icon medium>
                                    <FontAwesomeIcon icon={['far', 'chart-bar']} />
                                </Icon> <span>Open</span>
                            </NavLink>
                            <LoggedInOnly>
                                <a className={"navbar-item"} href={"https://join.slack.com/t/makerlog/shared_invite/enQtMzk4OTU5MjM1NjM0LTlmMjU4ZTQwODdmYTYxYTgyYTkwYTEzYTc1MzljZjE3N2QxNTg0MmUzYzAxNGUxOWMxY2NiNmFjM2Q1NTFmYzY"} rel="noopener noreferrer" target={'_blank'}>
                                    <Icon medium>
                                        <FontAwesomeIcon icon={['fab', 'slack']} />
                                    </Icon> Chat
                                </a>
                            </LoggedInOnly>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}

Navigation.propTypes = {}

export default Navigation;