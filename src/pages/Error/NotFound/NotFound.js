import React from 'react';
import { Hero, Container, Title, SubTitle, Button } from 'vendor/bulma'
import {Link} from "react-router-dom";
import Emoji from "../../../components/Emoji";
import Page from 'layouts/Page';

class NotFound extends React.Component {
    render() {
        return (
            <Page contained={false} className="NotFound">
                <Hero fullheight dark centered style={{backgroundImage: "url(https://pre00.deviantart.net/0519/th/pre/f/2011/187/1/0/nyan_cat_background_by_kento1-d3l6i50.jpg)"}}>
                    <Hero.Body>
                        <Container>
                            <Title>
                                We couldn't find that.
                            </Title>
                            <SubTitle>
                                <Emoji emoji={"🤖"} /> Beep boop. Error 404.
                            </SubTitle>
                            <Link to={"/"}>
                                <Button dark inverted outlined>Go home &raquo;</Button>
                            </Link>
                        </Container>
                    </Hero.Body>
                </Hero>
            </Page>
        )
    }
}

NotFound.propTypes = {}

export default NotFound;