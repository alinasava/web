export default {
    STREAM_WEEKS_TO_LOAD: 2,
    BASE_URL: process.env.REACT_APP_BASE_URL
}