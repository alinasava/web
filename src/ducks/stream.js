import {uniqBy} from 'lodash-es';
import { errorArray } from "../lib/utils/error";

const initialState = {
    isFollowingFeed: false,
    initialLoaded: false,
    isSyncing: true,
    fetchFailed: false,
    data: [],
    allLoaded: false,
    nextUrl: null,
    lastUpdatedTime: null,
    errorMessages: null,
}

export const types = {
    STREAM_INIT: 'STREAM_INIT',  
    STREAM_FETCH_REQUEST: 'STREAM_FETCH_REQUEST',
    STREAM_FETCH_SUCCEED: 'STREAM_FETCH_SUCCEED',
    STREAM_FETCH_FAILED: 'STREAM_FETCH_FAILED',
    STREAM_ALL_LOADED: 'STREAM_ALL_LOADED',
    STREAM_POLL_BEGIN: 'STREAM_POLL_BEGIN',
    STREAM_POLL_FORCE: 'STREAM_POLL_FORCE',
    STREAM_MERGE: 'STREAM_MERGE',
    STREAM_POLL_END: 'STREAM_POLL_END',
    STREAM_SOCKET_OPEN: 'STREAM_SOCKET_OPEN',
    STREAM_SOCKET_SYNC: 'STREAM_SOCKET_SYNC',
    STREAM_SOCKET_CLOSE: 'STREAM_SOCKET_CLOSE',
    STREAM_UPDATE_TASK: 'STREAM_UPDATE_TASK',
    STREAM_REMOVE_TASK: 'STREAM_REMOVE_TASK',
    STREAM_SET_LAST_UPDATED: 'STREAM_SET_LAST_UPDATED',
    STREAM_TOGGLE_FOLLOWING: 'STREAM_TOGGLE_FOLLOWING',
}

export const streamReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.STREAM_INIT:
            return {
                ...initialState,
                isFollowingFeed: state.isFollowingFeed,
            }

        case types.STREAM_TOGGLE_FOLLOWING:
            return {
                ...initialState,
                isFollowingFeed: !state.isFollowingFeed,
            }

        case types.STREAM_FETCH_REQUEST:
            return {
                ...state,
                isSyncing: true,
                fetchFailed: false
            }

        case types.STREAM_FETCH_SUCCEED:
            return {
                ...state,
                initialLoaded: true,
                isSyncing: false,
                fetchFailed: false,
                data: uniqBy([...action.data, ...state.data], 'id'),
                nextUrl: action.nextUrl
            }

        case types.STREAM_UPDATE_TASK:
            const updatedData = state.data.map(item => {
                if(item.id === action.id){
                    return { ...item, ...action.payload }
                }
                return item
            })
            return {
                ...state,
                data: updatedData
            }

        case types.STREAM_REMOVE_TASK:
            const removedTasks = state.data.filter(t => t.id !== action.id)
            return {
                ...state,
                data: removedTasks
            }

        case types.STREAM_FETCH_FAILED:
            return {
                ...state,
                isSyncing: false,
                fetchFailed: false,
                errorMessages: action.errorMessages
            }

        case types.STREAM_ALL_LOADED:
            return {
                ...state,
                initialLoaded: true,
                allLoaded: true,
                isSyncing: false,
            }

        case types.STREAM_MERGE:
            let lastUpdatedTime = null;
            if (action.data) {
                lastUpdatedTime = new Date(Math.max.apply(null, action.data.map(function(e) {
                    return new Date(e.updated_at);
                })));
            }
            return {
                ...state,
                initialLoaded: true,
                fetchFailed: false,
                allLoaded: false,
                lastUpdatedTime: lastUpdatedTime,
                data: uniqBy([...action.data, ...state.data], 'id'),
            }

        case types.STREAM_SET_LAST_UPDATED:
            return {
                ...state,
                lastUpdatedTime: action.lastUpdatedTime
            }

        default:
            return state
    }
}

export const actions = {
    init: () => ({ type: types.STREAM_INIT }),
    loadMore: () => {
        return {
            type: types.STREAM_FETCH_REQUEST
        }
    },
    streamSuccess: (data, nextUrl = null) => {
        return {
            type: types.STREAM_FETCH_SUCCEED,
            data: data,
            nextUrl: nextUrl
        }
    },
    streamFailed: (errorMessages = ['Failed to fetch stream.']) => {
        return {
            type: types.STREAM_FETCH_FAILED,
            errorMessages: errorArray(errorMessages),
        }
    },
    streamAllLoaded: () => {
        return {
            type: types.STREAM_ALL_LOADED
        }
    },
    removeTask: (id) => ({ type: types.STREAM_REMOVE_TASK, id: id }),
    updateTask: (id, payload={}) => ({ type: types.STREAM_UPDATE_TASK, id: id, payload: payload }),
    // legacy
    beginPolling: () => ({ type: types.STREAM_SOCKET_OPEN }),
    pollNow: () => ({ type: types.STREAM_POLL_FORCE }),
    endPolling: () => ({ type: types.STREAM_SOCKET_CLOSE }),
    connect: () => ({ type: types.STREAM_SOCKET_OPEN }),
    sync: () => ({ type: types.STREAM_SOCKET_SYNC }),
    disconnect: () => ({ type: types.STREAM_SOCKET_CLOSE }),
    merge: (data) => ({ type: types.STREAM_MERGE, data: data }),
    setLastUpdatedTime: (lastUpdatedTime) => ({ type: types.STREAM_SET_LAST_UPDATED, lastUpdatedTime: lastUpdatedTime }),
    toggleStreamType: () => ({ type: types.STREAM_TOGGLE_FOLLOWING }),
}