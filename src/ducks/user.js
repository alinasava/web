const initialState = {
    isLoading: false,
    failed: false,
    me: {},
}

export const types = {
    USER_REQUESTED: "USER_REQUESTED",
    USER_SUCCESS: "USER_SUCCESS",
    USER_FAILED: "USER_FAILED",
    USER_CHANGE_ACCENT: "USER_CHANGE_ACCENT",
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.USER_REQUESTED:
            return {
                ...state,
                isLoading: true,
                failed: false,
            }

        case types.USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                failed: false,
                me: action.user,
            }

        case types.USER_FAILED:
            return {
                ...state,
                isLoading: false,
                failed: true,
                me: {}
            }

        case types.USER_CHANGE_ACCENT:
            return {
                ...state,
                me: {
                    ...state.me,
                    accent: action.accent
                }
            }

        default:
            return state
    }
}

export const actions = {
    loadUser: () => {
        return {
            type: types.USER_REQUESTED
        }
    },

    userSuccess: (user, stats) => {
        return {
            type: types.USER_SUCCESS,
            user: user,
            stats: stats,
        }
    },

    userFailed: (e = null) => {
        return {
            type: types.USER_FAILED
        }
    },

    changeAccent: (hex) => ({
        type: types.USER_CHANGE_ACCENT,
        accent: hex
    })
}

export const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export const mapStateToProps = (state) => {
    return {
        isLoading: state.user.isLoading,
        failed: state.user.failed,
        me: state.user.me,
    }
}