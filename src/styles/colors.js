import chroma from 'chroma-js';

export function darker(hex, amount=0.8) {
   return chroma(hex).darken(amount)
}

export function lighter(hex, amount=0.8) {
    return chroma(hex).brighten(amount)
}

export class Colors {
    constructor(primary='#47E0A0') {
        this.primary = primary;
        this.primaryDarker = darker(this.primary).hex();
    }
}

// Expose a simple API - direct access to properties
export default Colors