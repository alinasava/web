import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import {Box, Button, Content, Image, Level, Media} from "vendor/bulma";
import Emoji from "components/Emoji";
import {Entry} from "features/stream";
import TimeAgo from 'react-timeago';
import {Product} from "features/products";


const Notification = ({ notification }) => {
    let notificationImage = null;
    let notificationHtml = 'Error parsing notification.';

    try {
        switch (notification.key) {
            case 'received_praise':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"👏"} /> You've got praise!</strong> <br />

                        <Content>
                            <p>
                                <Link to={`/@${notification.actor.username}`}>@{notification.actor.username}</Link> {notification.verb}.
                            </p>
                        </Content>

                        <Entry task={notification.target} /><br />
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'followed':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"👀"} /> You got a follower</strong> <br />
                        <Content>
                            <p>
                                <Link to={`/@${notification.actor.username}`}>
                                    @{notification.actor.username}
                                </Link> {notification.verb}.
                            </p>
                        </Content>
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'user_joined':
                notificationImage = '/assets/img/icon.jpg';
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"🎉"} /> Welcome to Makerlog, {notification.recipient.username}!</strong> <br />
                        <Content>
                            <p>
                                We're glad to have you here. Here's a few links to get you started.
                            </p>
                        </Content>
                        <div className={"buttons"}>
                            <Link to={'/explore'} className={"button is-primary"}>
                                Explore Makerlog
                            </Link>
                            <a className={"button"}
                               target={'_blank'}
                               rel="noopener noreferrer"
                               href={"https://makerskitchen.xyz"}>
                                Join Slack
                            </a>
                            <Link to={'/settings'} className={"button"}>
                                Edit your bio
                            </Link>
                        </div>
                    </div>
                )
                break;

            case 'broadcast':
                notificationImage = '/assets/img/icon.jpg';
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"📡"} /> Message from Makerlog</strong>
                        <Content>
                            <p>
                                {notification.verb}
                            </p>
                        </Content>
                        {notification.broadcast_link &&
                        <div>
                            <a className={"button is-small"} target="_blank" rel="noopener noreferrer" href={notification.broadcast_link}>
                                View link
                            </a>
                        </div>
                        }
                    </div>
                );
                break;

            case 'thread_created':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"✏️"} /> @{notification.actor.username} posted a topic </strong> <br />
                        <Content>
                            <p>
                                @{notification.actor.username} posted a topic titled "{notification.target.title}".
                            </p>
                        </Content>
                        <Level>
                            <Level.Left>
                                <Level.Item>
                                    <Link to={`/discussions/${notification.target.slug}`}>
                                        <Button>View thread</Button>
                                    </Link>
                                </Level.Item>
                            </Level.Left>
                        </Level>
                    </div>
                )
                break;

            case 'thread_replied':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"✏️"} /> @{notification.actor.username} replied to <Link to={`/discussions/${notification.target.slug}`}>{notification.target.title}</Link> </strong> <br />
                        <Content>
                            <p>
                                @{notification.actor.username} replied to a thread you're in.
                            </p>
                        </Content>
                        <Level>
                            <Level.Left>
                                <Level.Item>
                                    <Link to={`/discussions/${notification.target.slug}`}>
                                        <Button>View thread</Button>
                                    </Link>
                                </Level.Item>
                            </Level.Left>
                        </Level>
                    </div>
                )
                break;

            case 'task_commented':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"✏️"} /> @{notification.actor.username} replied to <Link to={`/tasks/${notification.target.id}`}>a task.</Link> </strong> <br />
                        <Content>
                            <Entry task={notification.target} />
                        </Content>
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'product_launched':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"🚀️"} /> @{notification.actor.username} launched <Link to={`/products/${notification.target.slug}`}>a product.</Link> </strong> <br />
                        <p>
                            <Product media product={notification.target} />
                        </p>
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'product_created':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"🚀️"} /> @{notification.actor.username} added <Link to={`/products/${notification.target.slug}`}>a product.</Link> </strong> <br />
                        <p>
                            <Product media product={notification.target} />
                        </p>
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'user_mentioned':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"📣"} /> {notification.actor.username} mentioned you.</strong> <br />

                        <Content>
                            <p>
                                <Link to={`/@${notification.actor.username}`}>@{notification.actor.username}</Link> {notification.verb}.
                            </p>
                        </Content>

                        <Entry task={notification.target} /><br />
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            case 'mention_discussion':
                notificationImage = notification.actor.avatar;
                notificationHtml = (
                    <div>
                        <strong><Emoji emoji={"📣"} /> {notification.actor.username} mentioned you.</strong> <br />

                        <Content>
                            <p>
                                <Link to={`/@${notification.actor.username}`}>@{notification.actor.username}</Link> {notification.verb}.
                            </p>
                        </Content>

                        <Level>
                            <Level.Left>
                                <Level.Item>
                                    <Link to={`/discussions/${notification.target.slug}`}>
                                        <Button>View thread</Button>
                                    </Link>
                                </Level.Item>
                            </Level.Left>
                        </Level>
                        <small className={"has-text-grey"}><TimeAgo date={notification.created} /></small>
                    </div>
                )
                break;

            default:
                notificationImage = '/assets/img/icon.jpg';
                notificationHtml = notification.verb;
        }
    } catch (e) {
        notificationImage = '/assets/img/icon.jpg';
        notificationHtml = "Content deleted.";
    }

    return (
        <Box className={notification.read ? "Notification read" : "Notification"}>
            <Media>
                {notificationImage &&
                <Media.Left>
                    <Image className="img-circle" is='32x32' src={notificationImage} />
                </Media.Left>
                }
                <Media.Content>
                    <p>
                        {notificationHtml}
                    </p>
                </Media.Content>
            </Media>
        </Box>
    )
}

Notification.propTypes = {
    notification: PropTypes.object.isRequired,
}

export default Notification;