import React from 'react';
import Comment from "./Comment";
import {orderByDate} from "../../../lib/utils/tasks";

class CommentList extends React.Component {
    render() {
        if (!this.props.comments) {
            return null;
        }

        const comments = orderByDate(this.props.comments, 'asc');

        return comments.map((c) => (
            <Comment key={c.id} comment={c} />
        ))
    }
}

export default CommentList;