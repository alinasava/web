import React from 'react';
import PropTypes from 'prop-types';
import {Box, Button} from "vendor/bulma";
import {getComments} from "lib/tasks";
import {Scrollbars} from 'react-custom-scrollbars';
import {withCurrentUser} from "features/users";
import CommentList from "./CommentList";
import CommentInput from "./CommentInput";
import {StreamCard as Card} from "../../stream/components/Stream/components/StreamCard/styled";
import Emoji from "../../../components/Emoji";
import {Link} from "react-router-dom";


class CommentsBox extends React.Component {
    state = {
        loading: false,
        comments: null,
        failed: false,
    }

    componentDidMount() {
        this.getComments();
    }

    getComments = async () => {
        this.setState({loading: true,})
        try {
            const comments = await getComments(this.props.task.id);
            let failed = false;
            let loading = false;
            this.setState({ comments, failed, loading });
            if (this.commentBox) {
                this.commentBox.scrollToBottom()
            }
        } catch (e) {
            this.setState({ loading: false, failed: true })
        }
    }

    onCreate = (c) => {
        this.setState({ comments: [...this.state.comments, c] })
        if (this.commentBox) {
            this.commentBox.scrollToBottom()
        }
    }

    render() {
        return (
            <div>
                {this.state.failed &&
                    <Box>
                    Couldn't load comments. <Button onClick={this.getComments}>Retry</Button>
                    </Box>
                }
                <Card>
                    <Card.Content style={{padding: 25}}>
                        {this.state.comments && !this.props.isLoggedIn && this.state.comments.length === 0 &&
                            <center style={{margin: 30}}><strong>
                                No comments yet. <Link to={'/begin'}>Sign in or join</Link> to post a comment!
                            </strong></center>
                        }
                        {this.state.comments && this.props.isLoggedIn && this.state.comments.length === 0 &&
                            <center style={{margin: 30}}><strong>
                                No comments yet. Start the conversation!
                            </strong> <Emoji emoji={"👇"} /></center>
                        }
                        {this.state.comments && this.state.comments.length > 0 &&
                            <Scrollbars ref={el => this.commentBox = el} className={"CommentsBox"} autoHeight autoHeightMax={'60vh'} autoHide autoHideTimeout={1000} autoHideDuration={200}>
                                <CommentList comments={this.state.comments} />
                            </Scrollbars>
                        }
                    </Card.Content>
                    <Card.Footer>
                        <CommentInput taskId={this.props.task.id} onCreate={this.onCreate} isLoading={this.state.loading} />
                    </Card.Footer>
                </Card>
            </div>
        )
    }
}

CommentsBox.propTypes = {
    task: PropTypes.object,
}

export default withCurrentUser(CommentsBox);