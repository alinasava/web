import React from 'react';
import axios from "axios";
import {SubTitle} from "vendor/bulma";
import {uniqBy} from "lodash-es";
import ReconnectingWebSocket from 'reconnecting-websocket';

import Stream from '../../components/Stream';
import {socketUrl} from "../../../../lib/utils/random";

class WeeklyStream extends React.Component {
    initialState = {
        initialLoaded: false,
        allLoaded: false,
        isSyncing: true,
        tasks: [],
        nextUrl: null,
        failed: false,
    }

    state = this.initialState

    async componentDidMount() {
        await this.loadMore();
        this.connect();
    }

    componentWillUnmount() {
        this.disconnect()
    }

    connect = () => {
        this.socket = new ReconnectingWebSocket(socketUrl(this.props.indexUrl));
        this.socket.onopen = () => {
            console.log(`Makerlog: Established connection to ${this.props.indexUrl}.`)
        }
        this.socket.onmessage = this.onWsEvent
        this.socket.onclose = () => {
            console.log(`Makerlog: Closed connection to ${this.props.indexUrl}.`)
        }
    }

    onWsEvent = (event) => {
        const data = JSON.parse(event.data)
        switch (data.type) {
            case 'task.created':
            case 'task.updated':
            case 'task.sync':
                this.setState({
                    tasks: uniqBy([data.payload, ...this.state.tasks], 'id')
                });
                break;

            case 'task.deleted':
                this.setState({
                    tasks: this.state.tasks.filter(t => t.id !== data.payload.id)
                });
                break;

            default:
                return;
        }
    }

    disconnect = () => {
        if (this.socket) {
            this.socket.close()
        }
    }

    loadMore = async () => {
        try {
            let nextUrl = this.state.nextUrl;

            this.setState({ isSyncing: true })

            if (!this.state.initialLoaded && !nextUrl) {
                // get initial link
                const streamMetadata = await axios.get(this.props.indexUrl);

                nextUrl = streamMetadata.data.latest_url;
            }

            // we now have metadata. go ahead, let's ROLL!
            if (nextUrl) {
                // get the stream data
                const response = await axios.get(nextUrl);

                this.setState({
                    isSyncing: false,
                    failed: false,
                    hasMore: response.data.previous_url !== null,
                    tasks: uniqBy([...this.state.tasks, ...response.data.data], 'id'),
                    initialLoaded: true,
                    nextUrl: response.data.previous_url
                })
            } else {
                this.setState({ isSyncing: false, hasMore: false })
            }
        } catch (e) {
            this.setState({
                failed: true,
            })
        }
    }

    render() {
        return <Stream
            isSyncing={this.state.isSyncing}
            loadMore={this.loadMore}
            hasMore={!!this.state.nextUrl}
            tasks={this.state.tasks}
            noActivityComponent={
                this.props.noActivityComponent ? this.props.noActivityComponent : <center>
                    <SubTitle>There's nothing here yet.</SubTitle>
                </center>
            } />
    }
}

WeeklyStream.propTypes = {}

export default WeeklyStream;
