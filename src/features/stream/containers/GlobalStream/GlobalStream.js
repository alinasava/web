import React from 'react';
import WeeklyStream from "../WeeklyStream";

class GlobalStream extends React.Component {
    render() {
        return <WeeklyStream indexUrl={`/explore/stream/`} />
    }
}

GlobalStream.propTypes = {}

export default GlobalStream;
