import React from 'react';
import PropTypes from 'prop-types';
import {Media} from "vendor/bulma";

const Avatar = ({ url }) => (
    <Media.Left>
        <figure className="image is-48x48">
            <img className="img-circle" src={url} alt="User" />
        </figure>
    </Media.Left>
)

Avatar.propTypes = {
    url: PropTypes.string.isRequired,
}

export default Avatar;