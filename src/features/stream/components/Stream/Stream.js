import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'vendor/bulma';
import { CSSTransitionGroup } from 'react-transition-group';
import InfiniteScroll from 'react-infinite-scroll-component';
import StreamSection from './components/StreamSection/index';
import NoActivityCard from "../NoActivityCard";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { sortStreamByActivity } from 'lib/utils/tasks';
import StreamFinished from './components/StreamFinished/index';
import Spinner from "../../../../components/Spinner";

/**
 * Stream Component
 * @param tasks tasks to be displayed.
 * @param loadMore called on loadmore click or scroll.
 * @param isSyncing boolean indicating loading status.
 * @param hasMore boolean indicating if there is data to be loaded on server.
 */

class Stream extends React.Component {
    render() {
        const data = sortStreamByActivity(this.props.tasks);

        if (Object.keys(data).length === 0 && !this.props.hasMore && !this.props.isSyncing) {
            return this.props.noActivityComponent
        }

        return (
            <InfiniteScroll
                next={this.props.loadMore}
                hasMore={this.props.hasMore}
                style={{ overflow: 'none'}}>
                <CSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={200}>
                    {Object.keys(data).map(
                        (date) => <StreamSection
                            key={date}
                            position={Object.keys(data).indexOf(date)}
                            canSwitchType={this.props.canSwitchType}
                            isFollowingFeed={this.props.isFollowingFeed}
                            onSwitch={this.props.onSwitch}
                            date={date}
                            activityData={data[date]} />
                    )}
                </CSSTransitionGroup>

                {this.props.hasMore && !this.props.isSyncing &&
                    <center>
                        <Button onClick={this.props.loadMore}>
                            <FontAwesomeIcon icon={'fire'} /> &nbsp; Click to load more
                        </Button>
                    </center>
                }
                {this.props.isSyncing && <center><Spinner /></center>}
                {!this.props.hasMore && !this.props.isSyncing && <StreamFinished />}
            </InfiniteScroll>
        )
    }
}

Stream.propTypes = {
    tasks: PropTypes.array.isRequired,
    loadMore: PropTypes.func.isRequired,
    isSyncing: PropTypes.bool.isRequired,
    hasMore: PropTypes.bool.isRequired,
}

Stream.defaultProps = {
    noActivityComponent: <NoActivityCard/>,
    canSwitchType: false,
    isFollowingFeed: false,
    onSwitch: null,
}

export default Stream;