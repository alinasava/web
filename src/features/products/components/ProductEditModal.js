import {deleteProduct, editProduct, getProductBySlug} from "../../../lib/products";
import {isFunction} from "lodash-es";
import React from "react";
import {Button, Control, Field, Image, Message, SubTitle, Title} from "vendor/bulma";
import LaunchedToggle from "./LaunchedToggle";
import Modal from "../../../components/Modal/Modal";
import ProjectPicker from "../../projects/components/ProjectPicker";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {File} from 'vendor/bulma';
import Spinner from "../../../components/Spinner";

class ProductEditForm extends React.Component {
    state = {
        isLoading: true,
        isSubmitting: false,
        finished: false,
        name: '',
        description: '',
        launched: false,
        logo: null,
        logoPreviewUrl: null,
        selectedProjects: [],
        url: '',
        productHunt: '',
        twitter: '',
        errorMessages: null,
    }

    fetchProduct = async () => {
        try {
            const product = await getProductBySlug(this.props.productSlug);
            this.setState({
                isLoading: false,
                name: product.name,
                description: product.description,
                launched: product.launched,
                logo: '',
                logoPreviewUrl: product.icon,
                url: product.website,
                productHunt: product.product_hunt,
                twitter: product.twitter,
                selectedProjects: product.projects.map(project => project.id)
            })
        } catch (e) {
            // ??
        }
    }

    componentDidMount() {
        this.fetchProduct()
    }

    onLogoUpload = (event) => {
        const file = event.target.files[0];
        const reader  = new FileReader();

        reader.addEventListener("load", (e) => {
            this.setState({
                logoPreviewUrl: reader.result
            })
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }

        this.setState({
            logo: file
        })
    }

    onSubmit = async () => {
        try {
            this.setState({ isSubmitting: true });
            await editProduct(
                this.props.productSlug,
                this.state.name,
                this.state.description,
                this.state.selectedProjects,
                this.state.productHunt,
                this.state.twitter,
                this.state.url,
                this.state.launched,
                this.state.logo
            )

            if (isFunction(this.props.onFinish)) {
                this.props.onFinish()
            }
        } catch (e) {
            this.setState({ isSubmitting: false, errorMessages: e.field_errors || e.message });
        }
    }

    onDelete = async () => {
        await deleteProduct(this.props.productSlug);

        if (isFunction(this.props.onDelete)) {
            this.props.onDelete()
        }
    }

    setUrl = (key, url) => {
        let newUrl = url;

        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            newUrl = `https://${url}`
        }
        this.setState({
            [key]: newUrl
        })
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            messages = this.state.errorMessages;
        }

        return messages
    }

    render() {
        if (this.state.isLoading) {
            return <Spinner small={true} />
        }

        return (
            // todo: refactor this classname!
            <div className="CreateProductForm">
                <Title is={"4"}>Editing {this.state.name}</Title>
                <hr />
                <Field>
                    <SubTitle is="6">Product name</SubTitle>
                    <Control>
                        <input value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Product description</SubTitle>
                    <Control>
                        <textarea value={this.state.description} onChange={(e) => this.setState({ description: e.target.value })} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Website</SubTitle>
                    <Control>
                        <input value={this.state.url} onChange={(e) => this.setUrl('url', e.target.value)} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Product Hunt URL</SubTitle>
                    <Control>
                        <input value={this.state.productHunt} onChange={(e) => this.setUrl('productHunt', e.target.value)} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Twitter handle</SubTitle>
                    <Control>
                        <input value={this.state.twitter} onChange={(e) => this.setState({ twitter: e.target.value })} />
                    </Control>
                </Field>
                <br />

                <div className={"columns"}>
                    <div className={"column"}>
                        <Field>
                            <SubTitle is="5">Launched?</SubTitle>
                            <Control>
                                <LaunchedToggle launched={this.state.launched} onLaunchedChange={(e) => this.setState({ launched: !this.state.launched })} />
                            </Control>
                        </Field>
                    </div>
                    <div className={"column"}>
                        <SubTitle is="5">Delete product</SubTitle>
                        <Button danger onClick={this.onDelete}>Delete "{this.state.name}"</Button>
                    </div>
                </div>
                <hr />
                <ProjectPicker
                    onProjectSelect={(projects) => this.setState({selectedProjects: projects})}
                    initialSelectedProjects={this.state.selectedProjects} />
                <hr />
                <div className="columns">
                    <div className="column">
                        <SubTitle is="5">Icon</SubTitle>
                        <br />
                        <File name boxed>
                            <File.Label>
                                <File.Input accept="image/*" onChange={this.onLogoUpload} />
                                <File.Cta>
                                    <File.Icon>
                                        <FontAwesomeIcon icon={'upload'} />
                                    </File.Icon>
                                    <File.Label as='span'>
                                        Choose a file…
                                    </File.Label>
                                </File.Cta>
                            </File.Label>
                        </File>
                    </div>
                    <div className="column">
                        <SubTitle is="5">Logo preview</SubTitle>
                        <br />
                        {this.state.logoPreviewUrl ?
                            <Image is={"128x128"} src={this.state.logoPreviewUrl} />
                            :
                            <FontAwesomeIcon icon={'ship'} size={"6x"} />
                        }
                    </div>
                </div>
                <hr />
                {this.state.errorMessages &&
                <Message danger>
                    <Message.Body>
                        {this.renderErrorMessages()}
                    </Message.Body>
                </Message>
                }
                <Button loading={this.state.isSubmitting} primary large onClick={this.onSubmit}>Submit changes</Button>
            </div>
        )
    }
}

const ProductEditModal = (props) => (
    <Modal
        open={props.open}
        onClose={props.onClose}
        percentWidth={60}>
        <Modal.Content verticallyCentered={true}>
            <ProductEditForm productSlug={props.productSlug} onDelete={props.onDelete} onFinish={props.onFinish} />
        </Modal.Content>
    </Modal>
);

export default ProductEditModal;