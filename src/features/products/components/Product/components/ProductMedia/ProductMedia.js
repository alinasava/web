import React from 'react';
import PropTypes from 'prop-types';
import {Level, Media, Tag, Button, Icon} from "vendor/bulma";
import {Link} from "react-router-dom";
import Emoji from "../../../../../../components/Emoji";
import {getHostname, normalizeUrl} from "../../../../../../lib/utils/products";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

class ProductMedia extends React.Component {
    renderMedium() {
        return (
            <div className={"ProductMedia"} onClick={() => this.props.navigate(`/products/${this.props.product.slug}`)}>
                <Media>
                    {this.props.product.icon &&
                        <Media.Left>
                            <figure className="image is-48x48 img-rounded">
                                <img src={this.props.product.icon} alt="User" />
                            </figure>
                        </Media.Left>
                    }
                    <Media.Content>
                        <div>
                            <Link to={`/products/${this.props.product.slug}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                                <strong>{this.props.product.name} {this.props.product.launched && <Tag><Emoji emoji={"🚀"} /> Launched</Tag>}</strong>
                            </Link>
                            <br />
                            <p>
                                {this.props.product.description}
                            </p>
                            <Level compact>
                                <Level.Left>
                                    {this.props.product.website &&
                                        <Level.Item>
                                            <a target="_blank" rel="noopener noreferrer" href={normalizeUrl(this.props.product.website)}>
                                                <Button small text>
                                                    <Icon>
                                                        <FontAwesomeIcon icon={'globe'} />
                                                    </Icon>
                                                    {getHostname(this.props.product.website)}
                                                </Button>
                                            </a>
                                        </Level.Item>
                                    }
                                </Level.Left>
                            </Level>
                        </div>
                    </Media.Content>
                </Media>
            </div>
        )
    }

    render() {
        if (this.props.medium) {
            return this.renderMedium()
        }

        return (
            <div className={"ProductMedia"}>
                <Link to={`/products/${this.props.product.slug}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                    <Media>
                        <Media.Left>
                            <figure className="image is-48x48 img-rounded">
                                <img src={this.props.product.icon ? this.props.product.icon : "https://via.placeholder.com/200?text=No+icon"} alt="Product" />
                            </figure>
                        </Media.Left>
                        <Media.Content>
                            <div>
                                <strong>
                                    {this.props.product.name} {this.props.product.launched && <Tag><Emoji emoji={"🚀"} /> Launched</Tag>}
                                </strong>
                                <br />
                                <small>
                                    {this.props.product.description}
                                </small>
                            </div>
                        </Media.Content>
                    </Media>
                </Link>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    navigate: (route) => dispatch(push(route))
})

ProductMedia.propTypes = {
    product: PropTypes.object.isRequired,
}

export default connect(
    null,
    mapDispatchToProps
)(ProductMedia);