import React from 'react';
import PropTypes from 'prop-types';
import {Card, Heading, Image, Level, Media, SubTitle, Tag, Title} from "vendor/bulma";
import Emoji from "../../../../../../components/Emoji";
import SocialMediaLevel from "../../../../../../components/SocialMediaLevel";
import {UserMedia, UserContainer} from "features/users";

class ProductHero extends React.Component {
    renderHero = (product) => (
        <section className="hero is-primary">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column ProductHero-Media">
                            <Media>
                                <Media.Left className="ProductHero-Icon">
                                    <Image className={"img-rounded"} is={"128x128"} src={product.icon ? product.icon : "http://via.placeholder.com/500?text=No+icon"} />
                                </Media.Left>
                                <Media.Content>
                                    <Title is="2">
                                        {product.name} {product.launched && <Tag><Emoji emoji={"🚀"} /> Launched</Tag>}
                                    </Title>
                                    <SubTitle>
                                        {product.description ? product.description : "The maker didn't describe this product... poor product."}
                                    </SubTitle>
                                    <SocialMediaLevel
                                        website={product.website}
                                        twitterUser={product.twitter}
                                        productHuntUrl={product.product_hunt}
                                    />
                                </Media.Content>
                            </Media>
                        </div>
                        <div className="column ProductHero-Stats">
                            <Level>
                                <Level.Item hasTextCentered>
                                    <Heading>Made by</Heading>
                                    <Card>
                                        <Card.Content>
                                            <UserContainer id={product.user} component={UserMedia} />
                                        </Card.Content>
                                    </Card>
                                </Level.Item>
                            </Level>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )

    render() {
        return this.renderHero(this.props.product)
    }
}

ProductHero.propTypes = {
    product: PropTypes.object.isRequired,
}

export default ProductHero;