import React from 'react';
import Tiles, {Tile} from "../../../components/Tiles";

const ProductGrid = ({ children }) => (
    <Tiles is={3}>
        {React.Children.toArray(children).map(c => <Tile>{c}</Tile>)}
    </Tiles>
)

export default ProductGrid