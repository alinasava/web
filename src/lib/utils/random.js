export function validateEmail(email) {
    var re = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    return re.test(String(email).toLowerCase());
}

export function socketUrl(path) {
    return `${process.env.REACT_APP_WS_URL}${path}`;
}