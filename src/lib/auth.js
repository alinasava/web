import axios from 'axios';
import {prettyAxiosError} from "./utils/error";

async function getToken(username, password) {
    try {
        const payload = { username: username, password: password }
        const response = await axios.post('/api-token-auth/', payload);
        return response.data['token'];
    } catch (e) {
        // return a pretty error
        prettyAxiosError(e)
    }
}

async function me() {
    const endpoint = 'me';
    const { data } = await axios.get(endpoint);
    return data
}

export {
    getToken,
    me
};