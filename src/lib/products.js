import axios from 'axios';
import { prettyAxiosError } from "./utils/error";

export async function getProducts(userId=null) {
    try {
        let endpoint = '/products/';
        if (userId) {
            endpoint = `/users/${userId}/products/`
        }
        const response = await axios.get(endpoint);
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}

export async function getMyProducts() {
    try {
        const response = await axios.get('/products/me/');
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}


export async function getProductBySlug(slug) {
    try {
        const response = await axios.get(`/products/${slug}/`);
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}

export async function deleteProduct(slug) {
    try {
        const response = await axios.delete(`/products/${slug}/`);
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}

export async function getRecentlyLaunched() {
    try {
        const response = await axios.get(`/products/recently_launched/`);
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}

//TODO: Refactor to object passing all params.
export async function createProduct(
    name,
    description,
    projects,
    productHunt,
    twitter,
    website,
    launched,
    icon
) {
    try {
        let data = new FormData();
        const headers = {
            'Content-Type': 'multipart/form-data'
        }
        if (icon !== null) {
            data.append("icon", icon);
        }
        data.append("name", name);
        data.append('product_hunt', productHunt);
        data.append('twitter', twitter);
        data.append('description', description);
        data.append('website', website);
        data.append('launched', launched);
        for (var i = 0; i < projects.length; i++) {
            data.append('projects', projects[i]);
        }
        let url = '/products/';
        const response = await axios.post(url, data, { headers })
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}

export async function editProduct(
    slug,
    name,
    description,
    projects,
    productHunt,
    twitter,
    website,
    launched,
    icon
) {
    try {
        let data = new FormData();
        const headers = {
            'Content-Type': 'multipart/form-data'
        }
        if (icon !== null) {
            data.append("icon", icon);
        }
        data.append("name", name);
        data.append('product_hunt', productHunt);
        data.append('twitter', twitter);
        data.append('description', description);
        data.append('website', website);
        data.append('launched', launched);
        for (var i = 0; i < projects.length; i++) {
            data.append('projects', projects[i]);
        }

        let url = `/products/${slug}/`;
        const response = await axios.patch(url, data, { headers })
        return response.data;
    } catch (e) {
        prettyAxiosError(e)
    }
}